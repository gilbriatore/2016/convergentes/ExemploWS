package br.edu.up;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.ws.rs.ext.RuntimeDelegate;

import com.sun.net.httpserver.HttpServer;  //Classes restritas
import com.sun.net.httpserver.HttpHandler; //Classes restritas


public class Servidor {
	
	public static void main(String[] args) throws Exception {

		//Criar o servidor http
		InetSocketAddress isa = new InetSocketAddress(9090);
		@SuppressWarnings("restriction")
		HttpServer server = HttpServer.create(isa,0); 
		
		//Cria o webservice
		WebServices webServices = new WebServices();
		@SuppressWarnings("restriction")
		HttpHandler handler = RuntimeDelegate.getInstance()
				.createEndpoint(webServices, HttpHandler.class);
		
		//Vincula o webservice com o servidor
		server.createContext("/", handler);
		server.start();
		
		System.out.println("Servidor rodando..."); 
		
	}
}